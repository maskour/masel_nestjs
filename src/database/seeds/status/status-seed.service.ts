import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { StatusesEnum } from '../../../statuses/statuses.enum';
import { Status } from '../../../statuses/entities/status.entity';

@Injectable()
export class StatusSeedService {
  constructor(
    @InjectRepository(Status)
    private repository: Repository<Status>,
  ) {}

  async run() {
    const count = await this.repository.count();

    if (count === 0) {
      await this.repository.save([
        this.repository.create({
          id: StatusesEnum.active,
          name: 'Active',
        }),
        this.repository.create({
          id: StatusesEnum.inactive,
          name: 'Inactive',
        }),
      ]);
    }
  }
}
