import { Module } from '@nestjs/common';
import { RoleSeedService } from './role-seed.service';
import { Role } from '../../../roles/entities/role.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Role])],
  providers: [RoleSeedService],
  exports: [RoleSeedService],
})
export class RoleSeedModule {}
