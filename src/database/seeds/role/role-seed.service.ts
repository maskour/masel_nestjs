import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RolesEnum } from '../../../roles/roles.enum';
import { Role } from '../../../roles/entities/role.entity';

@Injectable()
export class RoleSeedService {
  constructor(
    @InjectRepository(Role)
    private repository: Repository<Role>,
  ) {}

  async run() {
    const countUser = await this.repository.count({
      where: {
        id: RolesEnum.user,
      },
    });

    if (countUser === 0) {
      await this.repository.save(
        this.repository.create({
          id: RolesEnum.user,
          name: 'User',
        }),
      );
    }

    const countAdmin = await this.repository.count({
      where: {
        id: RolesEnum.admin,
      },
    });

    if (countAdmin === 0) {
      await this.repository.save(
        this.repository.create({
          id: RolesEnum.admin,
          name: 'Admin',
        }),
      );
    }
  }
}
