import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../../users/entities/user.entity';
import { Repository } from 'typeorm';
import { RolesEnum } from '../../../roles/roles.enum';
import { StatusesEnum } from '../../../statuses/statuses.enum';

@Injectable()
export class UserSeedService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  async run() {
    const countAdmin = await this.userRepository.count({
      where: {
        role: { id: RolesEnum.admin },
      },
    });

    if (countAdmin === 0) {
      await this.userRepository.save(
        this.userRepository.create({
          firstName: 'Super',
          lastName: 'Admin',
          email: 'admin@test.ma',
          password: 'admin',
          role: { id: RolesEnum.admin, name: 'Admin' },
          status: { id: StatusesEnum.active, name: 'Active' },
        }),
      );
    }

    const countUser = await this.userRepository.count({
      where: {
        role: { id: RolesEnum.user },
      },
    });

    if (countUser === 0) {
      await this.userRepository.save(
        this.userRepository.create({
          firstName: 'Maskour',
          lastName: 'Elhoucine',
          email: 'elhoucine.maskour7@gmail.com',
          password: 'maskour',
          role: { id: RolesEnum.user, name: 'User' },
          status: { id: StatusesEnum.active, name: 'Active' },
        }),
      );
    }
  }
}
