import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import databaseConfig from '../../config/database.config';
import appConfig from '../../config/app.config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeormConfigService } from '../typeorm-config.service';
import { DataSource } from 'typeorm';
import { UserSeedModule } from './user/user-seed.module';
import { StatusSeedModule } from './status/status-seed.module';
import { RoleSeedModule } from './role/role-seed.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [databaseConfig, appConfig],
      envFilePath: ['.env'],
    }),
    TypeOrmModule.forRootAsync({
      useClass: TypeormConfigService,
      dataSourceFactory: async (option) => {
        return await new DataSource(option).initialize();
      },
    }),
    UserSeedModule,
    StatusSeedModule,
    RoleSeedModule,
  ],
})
export class SeedModule {}
