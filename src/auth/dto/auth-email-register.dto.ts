import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsEmail, IsNotEmpty, MinLength, Validate } from 'class-validator';
import { IsNotExist } from '../../utils/validators/is-not-exists.validator';

export class AuthRegisterDto {
  @ApiProperty({ example: 'test@example.com' })
  @Transform(({ value }) => value.toLowerCase().trim())
  @Validate(IsNotExist, ['User'], {
    message: 'User with this email already exists',
  })
  @IsEmail()
  email: string;

  @ApiProperty()
  @MinLength(6)
  password: string;

  @ApiProperty({ example: 'Name' })
  @IsNotEmpty()
  firstName: string;

  @ApiProperty({ example: 'Test' })
  @IsNotEmpty()
  lastName: string;
}
