import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsNotEmpty, Validate } from 'class-validator';
import { IsExist } from '../../utils/validators/is-exists.validator';

export class AuthEmailLoginDto {
  @ApiProperty({ example: 'test@example.com' })
  @Transform(({ value }) => value.toLowerCase().trim())
  @Validate(IsExist, ['User'], {
    message: 'User with this email does not exist',
  })
  email: string;

  @ApiProperty({})
  @IsNotEmpty()
  password: string;
}
