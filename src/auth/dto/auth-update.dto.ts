import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, MinLength, Validate } from 'class-validator';
import { IsExist } from '../../utils/validators/is-exists.validator';
import { FileEntity } from '../../files/entities/file.entity';

export class AuthUpdateDto {
  @ApiProperty({ type: () => FileEntity })
  @IsOptional()
  @Validate(IsExist, ['FileEntity', 'id'], {
    message: 'File with id $value does not exist',
  })
  photo?: FileEntity;

  @ApiProperty({ example: 'Elhoucine' })
  @IsOptional()
  @IsNotEmpty({ message: 'First name should not be empty' })
  firstName?: string;

  @ApiProperty({ example: 'Maskour' })
  @IsOptional()
  @IsNotEmpty({ message: 'Last name should not be empty' })
  lastName?: string;

  @ApiProperty()
  @IsOptional()
  @IsNotEmpty()
  @MinLength(6)
  password?: string;

  @ApiProperty()
  @IsOptional()
  @IsNotEmpty({
    message: 'Old password should not be empty',
  })
  oldPassword: string;
}
