import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  UseGuards,
  Request,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthService } from '../services/auth.service';
import { AuthEmailLoginDto } from '../dto/auth-email-login.dto';
import { AuthRegisterDto } from '../dto/auth-email-register.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Auth')
@Controller({ path: 'auth', version: '1' })
export class AuthController {
  constructor(public authService: AuthService) {}

  @Post('login')
  @HttpCode(HttpStatus.OK)
  public async login(@Body() authLoginDto: AuthEmailLoginDto) {
    return this.authService.validateLogin(authLoginDto, false);
  }

  @Post('admin/login')
  @HttpCode(HttpStatus.OK)
  public async adminLogin(@Body() authLoginDto: AuthEmailLoginDto) {
    return this.authService.validateLogin(authLoginDto, true);
  }

  @Post('register')
  @HttpCode(HttpStatus.CREATED)
  async register(@Body() authRegisterDto: AuthRegisterDto) {
    return this.authService.register(authRegisterDto);
  }

  @ApiBearerAuth()
  @Get('me')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async me(@Request() request) {
    return this.authService.me(request.user);
  }
}
